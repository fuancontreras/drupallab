<?php

$aliases['lab.local'] = [
  'uri' => 'http://lab.lndo.site',
  'root' => '/app',
  'db-url' => 'mysql://drupal8:drupal8@database:3306/drupal8',
];

$aliases['lab.prod'] = [
  'uri' => 'https://drupal.hts-software.com',
  'remote-host' => 'hts-software.com',
  'remote-user' => 'htssoftw',
  'root' => '/home/htssoftw/drupal.hts-software.com',
  'ssh-options' => '-p 17177',
  'path-aliases' => [
    '%files' => 'files',
  ],
];