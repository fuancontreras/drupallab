<?php
$databases['default']['default'] = array (
  'database' => 'drupal8',
  'username' => 'drupal8',
  'password' => 'drupal8',
  'prefix' => '',
  'host' => 'database',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

$local_services_file = __DIR__ . '/services.local.yml';
if (file_exists($local_services_file)) {
  $settings['container_yamls'][] = $local_services_file;
}