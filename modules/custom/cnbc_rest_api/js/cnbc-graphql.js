(function($, Drupal) {
  "use strict";
  Drupal.behaviors.cnbc_rest_api = {
    attach: function(context) {
      jQuery(".cnbc-graphql-explorer-link", context).each(function() {
        $(this).removeClass("cnbc-graphql-explorer-link");
        $(this).addClass("cnbc-graphql-explorer-link--processed");
        $(this).on("click", function(evt) {
          evt.preventDefault();
          var query = "query=" + $(".cnbc-graphql--query").val();
          var variables = "&variables=" + $(".cnbc-graphql--vars").val();
          var newUrl = encodeURI("/graphql/explorer?" + query + variables);
          newUrl = newUrl.replace(/#/gi, "%23");
          var win = window.open(newUrl, "_blank");
          win.focus();
        });
      });
    }
  };
})(jQuery, Drupal);
