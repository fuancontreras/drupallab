<?php

namespace Drupal\cnbc_rest_api\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a CNBC Processor plugin item annotation object.
 *
 * @see \Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorManager
 * @see plugin_api
 *
 * @Annotation
 */
class CnbcApiProcessor extends Plugin {

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A human readable description of the processor.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * An example of the processor command.
   *
   * @var string
   *
   * @ingroup plugin_translatable
   */
  public $example;

}
