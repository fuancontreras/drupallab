<?php

namespace Drupal\cnbc_rest_api;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\graphql\GraphQL\Execution\QueryProcessor;
use Drupal\cnbc_rest_api\Controller\CnbcRequestController;
use GraphQL\Server\OperationParams;
use Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorManager;
use Drupal\Core\Cache\CacheableJsonResponse;

/**
 * Class CnbcRestApiService.
 */
class CnbcRestApiService {

  /**
   * Drupal\graphql\GraphQL\Execution\QueryProcessor definition.
   *
   * @var \Drupal\graphql\GraphQL\Execution\QueryProcessor
   */
  protected $graphqlQueryProcessor;

  /**
   * Drupal\cnbc_rest_api\Plugi\CnbcApiProcessorManager manager.
   *
   * @var \Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorManager
   */
  protected $processorPluginManager;

  /**
   * The service configuration parameters.
   *
   * @var array
   */
  protected $parameters;

  /**
   * Drupal\Core\Entity\EntityTypeManager manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $typeManager;

  /**
   * Constructs a new DefaultService object.
   *
   * @param \Drupal\graphql\GraphQL\Execution\QueryProcessor $processor
   *   The query processor.
   * @param \Drupal\Core\Entity\EntityTypeManager $type_manager
   *   Rest endpoints plugin manager.
   * @param \Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorManager $processor_plugin_manager
   *   Processor plugin manager.
   * @param array $parameters
   *   The service configuration parameters.
   */
  public function __construct(QueryProcessor $processor, EntityTypeManager $type_manager, CnbcApiProcessorManager $processor_plugin_manager, array $parameters) {
    $this->graphqlQueryProcessor = $processor;
    $this->typeManager = $type_manager;
    $this->processorPluginManager = $processor_plugin_manager;
    $this->parameters = $parameters;
  }

  /**
   * Execute GraphQL via function callback instead of HTTP request.
   *
   * @param string $graphql_query
   *   GraphQl query string.
   * @param mixed $replacements
   *   Key value array with graphql variables.
   * @param bool $readOnly
   *   Making the graphql query restricted to queries (not mutations).
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse
   *   Return GraphQL response.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function executeGraphQlQuery($graphql_query, $replacements = [], $readOnly = TRUE) {
    $controller = new CnbcRequestController($this->graphqlQueryProcessor, $this->parameters, $this);
    $params = [
      "query" => $graphql_query,
      "variables" => $replacements,
      "readOnly" => $readOnly,
    ];

    $query = OperationParams::create($params, FALSE);
    $query->variables = $replacements;
    $query->query = $graphql_query;

    return $controller->handleRequest('default:default', $query);
  }

  /**
   * Inject user defined fragments when required.
   *
   * @param string $query
   *   The GraphQL query.
   *
   * @return string
   *   The GraphQL query including the corresponding fragments.
   */
  public function injectStoredFragments(string $query): string {
    return $query;
  }

  /**
   * Execute annotation tags for GQL.
   *
   * @param \GraphQL\Server\OperationParams|\GraphQL\Server\OperationParams[] $operations
   *   The graphql operation(s) to execute.
   */
  public function executeGqlAnnotations(&$operations) {
    if (is_array($operations)) {
      foreach ($operations as $operation) {
        $this->processIncludeTags($operation);
      }
    }
    else {
      $this->processIncludeTags($operations);
    }
  }

  /**
   * Add includes replacements to the queries.
   *
   * Replaces the include annotations # @include(include_id) used in the
   * gql query with the associated values (which can contains additional
   * anotations themselves) until all the replacements have been performed.
   *
   * @param \GraphQL\Server\OperationParams $operation
   *   Query object for replacements.
   */
  public function processIncludeTags(OperationParams &$operation) {
    $include_entities = $this->typeManager->getStorage('graphql_include_config')
      ->loadMultiple();
    $replace_map = [];
    $search_map = [];
    foreach ($include_entities as $entity) {
      $key = $entity->id();

      $search_map[] = "@include($key)";
      $replace_map[] = $key . " \r\n " . $entity->query . "  \r\n ";
    }

    $processed_query = $operation->query;

    do {
      $replaced_string = $processed_query;
      $processed_query = str_replace($search_map, $replace_map, $processed_query);
    } while ($replaced_string !== $processed_query);

    $operation->query = $processed_query;
  }

  /**
   * Run post-processors.
   *
   * Loads the CnbcApiProcessor plugin instances and executes them based on the
   * gql annotations @output(processor_id) tags. The processors are only
   * executed once and the precedence matches the order of appereance in the
   * gql query.
   *
   * @param \GraphQL\Server\OperationParams|\GraphQL\Server\OperationParams[] $operations
   *   The graphql operation(s) to execute.
   * @param \Drupal\Core\Cache\CacheableJsonResponse $json_result
   *   The query result.
   *
   * @return \Drupal\Core\Cache\CacheableJsonResponse
   *   The modified query result.
   */
  public function loadGqlPostProcessors($operations, CacheableJsonResponse $json_result) {
    if (is_array($operations)) {
      return $json_result;
    }

    $query = $operations->query;
    $processors[] = [
      'id' => 'default_processor',
      'parameters' => [],
    ];
    $processor_plugin_definitions = $this->processorPluginManager->getDefinitions();
    $regex = '/\#.*@output\((?<operator>\w+),?(?<args>.*)\).*/m';

    preg_match_all($regex, $query, $matches, PREG_SET_ORDER, 0);

    foreach ($matches as $match) {
      $processor_id = $match['operator'] ?? FALSE;
      $processor_args = $match['args'] ?? '';
      if ($processor_id && array_key_exists($processor_id, $processor_plugin_definitions)) {
        $processors[] = [
          "id" => $processor_id,
          "parameters" => $processor_args === '' ? [] : explode(',', str_replace(' ', '', $processor_args)),
        ];
      }
    }

    // Execute processors with arguments.
    foreach ($processors as $processor) {
      $plugin = $this->processorPluginManager->createInstance($processor['id']);
      $plugin->processOutput($operations, $json_result, $processor['parameters']);
    }

    // Expose debug variables.
    $this->exposeDebugVariables($operations, $json_result);

    return $json_result;
  }

  /**
   * Injects debug variables.
   *
   * @param \GraphQL\Server\OperationParams|\GraphQL\Server\OperationParams[] $operations
   *   The graphql operation(s) to execute.
   * @param \Drupal\Core\Cache\CacheableJsonResponse $json_result
   *   GraphQL response.
   */
  public function exposeDebugVariables($operations, CacheableJsonResponse $json_result): void {
    $data = json_decode($json_result->getContent(), TRUE);
    if ($operations->variables['debug'] ?? FALSE) {
      if (array_keys($data) !== range(0, count($data) - 1)) {
        $data['debug']['variables'] = $operations->variables;
      }
      else {
        $data[] = ['debug' => ['variables' => $operations->variables]];
      }
      $json_result->setContent(json_encode($data));
    }
  }

}
