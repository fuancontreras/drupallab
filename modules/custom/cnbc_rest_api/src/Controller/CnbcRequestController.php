<?php

namespace Drupal\cnbc_rest_api\Controller;

use Drupal\graphql\Controller\RequestController;
use Drupal\graphql\GraphQL\Execution\QueryProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\cnbc_rest_api\CnbcRestApiService;

/**
 * Class CnbcRequestController.
 */
class CnbcRequestController extends RequestController {

  /**
   * The query processor.
   *
   * @var \Drupal\cnbc_rest_api\CnbcRestApiService
   */
  protected $endpointsApi;

  /**
   * RequestController constructor.
   *
   * @param \Drupal\graphql\GraphQL\Execution\QueryProcessor $processor
   *   The query processor.
   * @param array $parameters
   *   The service configuration parameters.
   * @param \Drupal\cnbc_rest_api\CnbcRestApiService $endpoints_api
   *   The service CNBC Endpoints.
   */
  public function __construct(QueryProcessor $processor, array $parameters, CnbcRestApiService $endpoints_api) {
    parent::__construct($processor, $parameters);
    $this->endpointsApi = $endpoints_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('graphql.query_processor'),
      $container->getParameter('graphql.config'),
      $container->get('cnbc_rest_api.connect')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function handleRequest($schema, $operations) {
    $this->endpointsApi->executeGqlAnnotations($operations);
    if (is_array($operations)) {
      return $this->endpointsApi->loadGqlPostProcessors($operations, $this->handleBatch($schema, $operations));
    }

    return $this->endpointsApi->loadGqlPostProcessors($operations, $this->handleSingle($schema, $operations));
  }

}
