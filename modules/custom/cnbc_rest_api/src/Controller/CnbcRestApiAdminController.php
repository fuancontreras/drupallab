<?php

namespace Drupal\cnbc_rest_api\Controller;

use Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorManager;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class CnbcRestApiAdminController.
 */
class CnbcRestApiAdminController extends ControllerBase {

  /**
   * Drupal\cnbc_rest_api\Plugi\CnbcApiProcessorManager manager.
   *
   * @var \Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorManager
   */
  protected $processorPluginManager;

  /**
   * Drupal\Core\Entity\EntityTypeManager manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $typeManager;

  /**
   * Constructs a new CnbcRestApiAdminController object.
   */
  public function __construct(CnbcApiProcessorManager $processor_plugin_manager, EntityTypeManager $type_manager) {
    $this->processorPluginManager = $processor_plugin_manager;
    $this->typeManager = $type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.cnbc_processors'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs an admin UI page.
   *
   * @return array
   *   Returns a render array for the admin page.
   */
  public function adminPage() {

    $build = [
      '#type' => 'container',
      '#title' => $this->t('CNBC GraphQL Endpoints & Includes Configuration'),
      '#attributes' => [
        'class' => [
          'container',
        ],
      ],
    ];

    $build['endpoints'] = $this->getEndpointsAdminSection();
    $build['includes'] = $this->getIncludesAdminSection();
    $build['processors'] = $this->getProcessorsAdminSection();

    return $build;
  }

  /**
   * Returns endpoints configuration section.
   */
  protected function getEndpointsAdminSection() {
    $container = [
      '#type' => 'container',
      '#title' => $this->t('GraphQL Endpoints'),
      '#attributes' => [
        'class' => [
          'graphql_endpoints',
          'clearfix',
        ],
      ],
    ];

    $container['endpoints'] = [
      '#type' => 'details',
      '#title' => $this->t('Endpoints'),
      '#description' => $this->t('Manage GraphQL endpoints definitions.'),
      '#open' => TRUE,
    ];

    $container['endpoints']['create_link'] = [
      '#type' => 'link',
      '#title' => $this->t('Create GraphQL endpoint'),
      '#url' => Url::fromRoute('entity.graphql_endpoint_config.add_form'),
      '#suffix' => '</li></ul>',
      '#prefix' => '<ul class="action-links"><li>',
      '#attributes' => [
        'class' => [
          'button button-action',
          'button--primary',
          'button--small',
        ],
      ],
    ];

    $table['endpoints-table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('ID'),
        $this->t('Description'),
        $this->t('Method'),
        $this->t('Url'),
        $this->t('Actions'),

      ],
    ];

    $endpoint_entities = $this->typeManager->getStorage('graphql_endpoint_config')
      ->loadMultiple();
    $i = 1;
    foreach ($endpoint_entities as $entity) {
      $id = $entity->id();
      $table['endpoints-table'][$i]['name'] = [
        '#type' => 'markup',
        '#markup' => $entity->id(),
      ];
      $table['endpoints-table'][$i]['label'] = [
        '#type' => 'markup',
        '#markup' => $entity->label(),
      ];
      $table['endpoints-table'][$i]['method'] = [
        '#type' => 'markup',
        '#markup' => 'GET',
      ];
      $table['endpoints-table'][$i]['url'] = [
        '#type' => 'markup',
        '#markup' => $entity->url,
      ];
      $table['endpoints-table'][$i]['extra_actions'] = [
        '#type' => 'dropbutton',
        '#links' => [
          'simple_form' => [
            'title' => $this
              ->t('Edit'),
            'url' => Url::fromRoute('entity.graphql_endpoint_config.edit_form', ['graphql_endpoint_config' => $id]),
          ],
          'demo' => [
            'title' => $this
              ->t('Delete'),
            'url' => Url::fromRoute('entity.graphql_endpoint_config.delete_form', ['graphql_endpoint_config' => $id]),
          ],
        ],
      ];
      $i++;
    }

    $container['endpoints']['table'] = $table;

    return $container;
  }

  /**
   * Returns includes configuration section.
   */
  protected function getIncludesAdminSection() {
    $container = [
      '#type' => 'container',
      '#title' => $this->t('Includes'),
      '#attributes' => [
        'class' => [
          'graphql_includes',
          'clearfix',
        ],
      ],
    ];

    $container['includes'] = [
      '#type' => 'details',
      '#title' => $this->t('Includes'),
      '#description' => $this->t('Manage GraphQL includes definitions.'),
      '#open' => TRUE,
    ];

    $container['includes']['create_link'] = [
      '#type' => 'link',
      '#title' => $this->t('Create GraphQL include'),
      '#url' => Url::fromRoute('entity.graphql_include_config.add_form'),
      '#suffix' => '</li></ul>',
      '#prefix' => '<ul class="action-links"><li>',
      '#attributes' => [
        'class' => [
          'button button-action',
          'button--primary',
          'button--small',
        ],
      ],
    ];

    $table['endpoints-table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('ID'),
        $this->t('Description'),
        $this->t('Usage (Copy and paste within GQL)'),
        $this->t('Actions'),

      ],
    ];

    $include_entities = $this->typeManager->getStorage('graphql_include_config')
      ->loadMultiple();
    $i = 1;
    foreach ($include_entities as $entity) {
      $id = $entity->id();
      $table['endpoints-table'][$i]['name'] = [
        '#type' => 'markup',
        '#markup' => $entity->id(),
      ];
      $table['endpoints-table'][$i]['label'] = [
        '#type' => 'markup',
        '#markup' => $entity->label(),
      ];
      $table['endpoints-table'][$i]['usage'] = [
        '#type' => 'markup',
        '#markup' => $this->t('# @include(@id)', ['@id' => $id]),
      ];
      $table['endpoints-table'][$i]['extra_actions'] = [
        '#type' => 'dropbutton',
        '#links' => [
          'simple_form' => [
            'title' => $this
              ->t('Edit'),
            'url' => Url::fromRoute('entity.graphql_include_config.edit_form', ['graphql_include_config' => $id]),
          ],
          'demo' => [
            'title' => $this
              ->t('Delete'),
            'url' => Url::fromRoute('entity.graphql_include_config.delete_form', ['graphql_include_config' => $id]),
          ],
        ],
      ];
      $i++;
    }

    $container['includes']['table'] = $table;

    return $container;
  }

  /**
   * Returns output processor configuration section.
   */
  protected function getProcessorsAdminSection() {
    $container = [
      '#type' => 'container',
      '#title' => $this->t('Output processors'),
      '#attributes' => [
        'class' => [
          'graphql_processors',
          'clearfix',
        ],
      ],
    ];

    $container['includes'] = [
      '#type' => 'details',
      '#title' => $this->t('Output processors'),
      '#open' => TRUE,
    ];

    $table['endpoints-table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('ID'),
        $this->t('Description'),
        $this->t('Usage Example'),
        $this->t('Transformation'),

      ],
    ];

    $i = 1;
    $processor_plugin_definitions = $this->processorPluginManager->getDefinitions();
    ksort($processor_plugin_definitions);

    foreach ($processor_plugin_definitions as $plugin_definition) {
      if (isset($plugin_definition['id']) && isset($plugin_definition['label'])
        && $plugin_definition['id'] !== 'default_processor') {

        $plugin = $this->processorPluginManager->createInstance($plugin_definition['id']);
        $table['endpoints-table'][$i]['name'] = [
          '#type' => 'markup',
          '#markup' => $plugin->getPluginId(),
        ];
        $table['endpoints-table'][$i]['label'] = [
          '#type' => 'markup',
          '#markup' => $plugin->getLabel(),
        ];

        /* As output processors plugin annotation example could use more
         * than one line the * character is removed so the annotation can use
         * multiple short (less than 80 characters) lines instead.
         */
        $table['endpoints-table'][$i]['usage'] = [
          '#type' => 'markup',
          '#markup' => str_replace('*', '', $plugin->getExample()),
        ];

        /* As output processors plugin annotation description could use more
         * than one line the * character is removed so the annotation can use
         * multiple short (less than 80 characters) lines instead.
         */
        $table['endpoints-table'][$i]['description'] = [
          '#type' => 'markup',
          '#markup' => str_replace('*', '', $plugin->getDescription()),
        ];
        $i++;
      }
    }

    $container['includes']['table'] = $table;

    return $container;
  }

}
