<?php

namespace Drupal\cnbc_rest_api\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the CNBC GraphQL Endpoint Configuration entity.
 *
 * @ConfigEntityType(
 *   id = "graphql_endpoint_config",
 *   label = @Translation("CNBC GraphQL Endpoint Configuration"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\cnbc_rest_api\CnbcGraphQlEndpointConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cnbc_rest_api\Form\EndpointConfigForm",
 *       "edit" = "Drupal\cnbc_rest_api\Form\EndpointConfigForm",
 *       "delete" = "Drupal\cnbc_rest_api\Form\EndpointConfigDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "graphql_endpoint_config",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/cnbc_rest_api/endpoints/graphql_endpoint_config/{graphql_endpoint_config}",
 *     "add-form" = "/admin/config/cnbc_rest_api/endpoints/graphql_endpoint_config/add",
 *     "edit-form" = "/admin/config/cnbc_rest_api/endpoints/graphql_endpoint_config/{graphql_endpoint_config}/edit",
 *     "delete-form" = "/admin/config/cnbc_rest_api/endpoints/graphql_endpoint_config/{graphql_endpoint_config}/delete",
 *     "collection" = "/admin/config/cnbc_rest_api/endpoints/graphql_endpoint_config"
 *   }
 * )
 */
class EndpointConfig extends ConfigEntityBase {

  /**
   * The CNBC GraphQL Endpoint Configuration ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The CNBC GraphQL Endpoint Configuration label.
   *
   * @var string
   */
  protected $label;

  /**
   * The GQL query.
   *
   * @var string
   */
  public $query;

  /**
   * The endpoint url.
   *
   * @var string
   */
  public $url;

  /**
   * The testing gql variables.
   *
   * @var string
   */
  public $testvars;

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    $rest_services = \Drupal::service('plugin.manager.rest');
    $rest_services->clearCachedDefinitions();
  }

}
