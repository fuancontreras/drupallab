<?php

namespace Drupal\cnbc_rest_api\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the CNBC GraphQL Include Configuration entity.
 *
 * @ConfigEntityType(
 *   id = "graphql_include_config",
 *   label = @Translation("CNBC GraphQL Include Config"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cnbc_rest_api\IncludeConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cnbc_rest_api\Form\IncludeConfigForm",
 *       "edit" = "Drupal\cnbc_rest_api\Form\IncludeConfigForm",
 *       "delete" = "Drupal\cnbc_rest_api\Form\IncludeConfigDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "graphql_include_config",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/cnbc_rest_api/includes/graphql_include_config/{graphql_include_config}",
 *     "add-form" = "/admin/config/cnbc_rest_api/includes/graphql_include_config/add",
 *     "edit-form" = "/admin/config/cnbc_rest_api/includes/graphql_include_config/{graphql_include_config}/edit",
 *     "delete-form" = "/admin/config/cnbc_rest_api/includes/graphql_include_config/{graphql_include_config}/delete",
 *     "collection" = "/admin/config/cnbc_rest_api/includes/graphql_include_config"
 *   }
 * )
 */
class IncludeConfig extends ConfigEntityBase {

  /**
   * The CNBC Include Config ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The CNBC Include Config label.
   *
   * @var string
   */
  protected $label;

  /**
   * The GQL query.
   *
   * @var string
   */
  public $query;

  /**
   * The testing gql variables.
   *
   * @var string
   */
  public $testvars;

  /**
   * The testing gql query.
   *
   * @var string
   */
  public $tesquery;

}
