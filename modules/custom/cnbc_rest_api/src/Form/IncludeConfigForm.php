<?php

namespace Drupal\cnbc_rest_api\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IncludeConfigForm.
 */
class IncludeConfigForm extends EntityForm {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new GraphQLCustomEndpointConfirmForm object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Constructs a Drupal\cnbc_rest_api\Form\GraphQLCustomEndpointForm object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The factory for configuration objects.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $graphql_include_config = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $graphql_include_config->label(),
      '#description' => $this->t("Label for the CNBC Include Config."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $graphql_include_config->id(),
      '#machine_name' => [
        'exists' => '\Drupal\cnbc_rest_api\Entity\IncludeConfig::load',
      ],
      '#disabled' => !$graphql_include_config->isNew(),
    ];

    $form['query'] = [
      '#type' => 'textarea',
      '#title' => $this->t('GQL Query'),
      '#default_value' => $graphql_include_config->query,
      '#required' => TRUE,
      '#rows' => 20,
    ];

    $form['test'] = [
      '#type' => 'details',
      '#title' => $this->t('Test & Preview'),
      '#description' => $this->t('Test & Preview your query using GraphQL Explorer.'),
      '#open' => TRUE,
    ];

    $form['test']['testquery'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Test GQL Query'),
      '#default_value' => $graphql_include_config->testquery ?? '',
      '#required' => FALSE,
      '#rows' => 10,
      '#attributes' => [
        'class' => [
          'cnbc-graphql--query',
        ],
      ],
    ];

    $form['test']['testvars'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Test GQL Variables'),
      '#description' => $this->t('GraphQL query used for testing the common.</a>'),
      '#default_value' => $graphql_include_config->testvars ?? '',
      '#required' => FALSE,
      '#rows' => 4,
      '#attributes' => [
        'class' => [
          'cnbc-graphql--vars',
        ],
      ],
    ];

    $form['test']['preview'] = [
      '#type' => 'link',
      '#title' => $this->t('Test your Query using GraphQL Explorer'),
      '#url' => Url::fromUserInput("/#graph-ql"),
      '#suffix' => '</li></ul>',
      '#prefix' => '<ul class="action-links"><li>',
      '#attributes' => [
        'target' => '_blank',
        'id' => ['test-link'],
        'class' => [
          'cnbc-graphql-explorer-link',
          'button button-action',
          'button--secundary',
          'button--small',
        ],
      ],
    ];

    $form['#attached'] = [
      'library' => [
        'cnbc_rest_api/cnbc-graphql',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $graphql_include_config = $this->entity;
    $status = $graphql_include_config->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger->addMessage($this->t('Created the %label CNBC Include Config.', [
          '%label' => $graphql_include_config->label(),
        ]));
        break;

      default:
        $this->messenger->addMessage($this->t('Saved the %label CNBC Include Config.', [
          '%label' => $graphql_include_config->label(),
        ]));
    }
    $form_state->setRedirectUrl(Url::fromRoute('cnbc_rest_api.graph_ql_endpoints_admin'));
  }

}
