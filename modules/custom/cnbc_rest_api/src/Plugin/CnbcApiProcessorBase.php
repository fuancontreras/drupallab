<?php

namespace Drupal\cnbc_rest_api\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Cache\CacheableJsonResponse;

/**
 * Base class for CNBC Processor plugins.
 */
abstract class CnbcApiProcessorBase extends PluginBase implements CnbcApiProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function processOutput($operations, CacheableJsonResponse $json_result, array $parameters = []);

  /**
   * {@inheritdoc}
   */
  public function getExample() {
    return $this->pluginDefinition['example'];
  }

}
