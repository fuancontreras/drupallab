<?php

namespace Drupal\cnbc_rest_api\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Cache\CacheableJsonResponse;

/**
 * Defines an interface for CNBC Processor plugins.
 */
interface CnbcApiProcessorInterface extends PluginInspectionInterface {

  /**
   * Provide a label of the processor.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   A readable title for the processor.
   */
  public function getLabel();

  /**
   * Provide HTML markup describing an example of the processor.
   *
   * @return string
   *   Markup string for the processor examples.
   */
  public function getExample();

  /**
   * Provide HTML markup describing the processor.
   *
   * @return string
   *   A description of the processor.
   */
  public function getDescription();

  /**
   * Process output response.
   *
   * @param \GraphQL\Server\OperationParams|\GraphQL\Server\OperationParams[] $operations
   *   The graphql operation(s) to execute.
   * @param \Drupal\Core\Cache\CacheableJsonResponse $json_result
   *   GraphQL response.
   * @param array $parameters
   *   Additional parameters included in the annotation.
   */
  public function processOutput($operations, CacheableJsonResponse $json_result, array $parameters = []);

}
