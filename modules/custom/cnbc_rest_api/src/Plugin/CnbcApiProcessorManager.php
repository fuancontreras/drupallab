<?php

namespace Drupal\cnbc_rest_api\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Processors plugin plugin manager.
 */
class CnbcApiProcessorManager extends DefaultPluginManager {

  /**
   * Constructs a new CnbcApiProcessor object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Processor', $namespaces, $module_handler, 'Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorInterface', 'Drupal\cnbc_rest_api\Annotation\CnbcApiProcessor');

    $this->alterInfo('cnbc_controller_plugin_info');
    $this->setCacheBackend($cache_backend, 'cnbc_processor_plugin_plugins');
  }

}
