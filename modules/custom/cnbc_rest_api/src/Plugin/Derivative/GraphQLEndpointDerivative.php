<?php

namespace Drupal\cnbc_rest_api\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides plugin definitions for GraphQL endpoints.
 */
class GraphQLEndpointDerivative extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $typeManager;

  /**
   * Constructs new NodeBlock.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $type_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManager $type_manager) {
    $this->typeManager = $type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $endpoint_entities = $this->typeManager->getStorage('graphql_endpoint_config')
      ->loadMultiple();
    foreach ($endpoint_entities as $entity) {
      $id = $entity->id();
      $label = $entity->label();
      if ($id && $label) {
        $this->derivatives[$id] = $base_plugin_definition;
        $this->derivatives[$id]["id"] = $id;
        $this->derivatives[$id]["label"] = new TranslatableMarkup("CNBC-GQL - @name", [
          '@name' => $label,
        ]);
        $this->derivatives[$id]["uri_paths"] = ['canonical' => $entity->url];
      }
    }
    return $this->derivatives;
  }

}
