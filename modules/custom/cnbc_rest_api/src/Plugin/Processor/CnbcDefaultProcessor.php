<?php

namespace Drupal\cnbc_rest_api\Plugin\Processor;

use Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorBase;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Default output processor.
 *
 * Provides a CnbcApiProcessor for output formatting from GraphQL response
 * structure to results rendering.
 *
 * @CnbcApiProcessor(
 *   id = "default_processor",
 *   label = @Translation("Renders results without GraphQL default structure."),
 *   description = @Translation("<p>Extract results from default GraphQL JSON
 *   structure. </p><p>Injects error details and debug information.</p>"),
 *   example = "This processor is added automatically."
 * )
 */
class CnbcDefaultProcessor extends CnbcApiProcessorBase {

  /**
   * {@inheritdoc}
   *
   * Adds url query caching so url (including query vars) are considered
   * as caching requirements. If query vars are not included endpoints could
   * return the wrong cached response. The method also extracts results for
   * standard (included) GraphQL queries and views based queries.
   */
  public function processOutput($operations, CacheableJsonResponse $json_result, array $parameters = []) {
    $build = [
      '#cache' => [
        'contexts' => ['url.query_args'],
        'tags' => ['config:rest.resource.cnbc_api'],
      ],
    ];
    $cache_metadata = CacheableMetadata::createFromRenderArray($build);
    $json_result->addCacheableDependency($cache_metadata);

    $this->extractDataResults($operations, $json_result);
    $this->extractViewsResults($operations, $json_result);
  }

  /**
   * Extract views results if available.
   *
   * @param \GraphQL\Server\OperationParams|\GraphQL\Server\OperationParams[] $operations
   *   The graphql operation(s) to execute.
   * @param \Drupal\Core\Cache\CacheableJsonResponse $json_result
   *   GraphQL response.
   */
  protected function extractViewsResults($operations, CacheableJsonResponse $json_result): void {
    $data = json_decode($json_result->getContent(), TRUE);
    if (is_array($data)) {
      if (count($data) == 1) {
        $data = array_pop($data);
        if ($data['results'] ?? FALSE) {
          $data = $data['results'];
          $json_result->setContent(json_encode($data));
        }
      }
    }
  }

  /**
   * Inject debug variables.
   *
   * @param \GraphQL\Server\OperationParams|\GraphQL\Server\OperationParams[] $operations
   *   The graphql operation(s) to execute.
   * @param \Drupal\Core\Cache\CacheableJsonResponse $json_result
   *   GraphQL response.
   */
  protected function extractDataResults($operations, CacheableJsonResponse $json_result): void {
    // Extract data.
    $data = json_decode($json_result->getContent(), TRUE);
    if (isset($data['data'])) {
      $json_result->setContent(json_encode($data['data']));
    }
  }

}
