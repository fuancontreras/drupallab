<?php

namespace Drupal\cnbc_rest_api\Plugin\Processor;

use Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorBase;
use Drupal\Core\Cache\CacheableJsonResponse;

/**
 * Provides a CnbcApiProcessor processor.
 *
 * @CnbcApiProcessor(
 *   id = "expand",
 *   label = @Translation("Replaces the field with the resolved hierarchy."))
 * )
 */
class CnbcFieldExpand extends CnbcApiProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return "<p>Replaces the field with the resolved hierarchy
    fields.</p> 
    <p> The hierarchy must resolve in an associative
    array/object.</p>";
  }

  /**
   * {@inheritdoc}
   */
  public function getExample() {
    return "<p># @output(<b>expand</b>, fieldWrittenBy, 0, entity)</p>";
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '<p>Input: <pre> 
    {
      "fieldWrittenBy": [
        entity: {
          "ordinal": 2,
          "targetId": 2,
        }
      ]
      "anotherField: 2  
    ]
    <pre>Output: <pre>
   {
      "ordinal": 2,
      "targetId": 2,
      "anotherField: 2, 
   }   
    </pre></p>';
  }

  /**
   * {@inheritdoc}
   */
  public function processOutput($operations, CacheableJsonResponse $json_result, array $parameters = []) {
    if (count($parameters) > 1) {
      $field_name = reset($parameters);
      $data = json_decode($json_result->getContent(), TRUE);
      $data = $this->expandTreeValue($data, $field_name, $parameters);
      $json_result->setContent(json_encode($data));
    }
  }

  /**
   * Finds a value in a multidimensional array given a hierarchy of keys.
   *
   * The returned value after checking the hierarchy must be an associative
   * array.
   *
   * @param mixed $tree
   *   Multidimensional array or value.
   * @param array $hierarchy
   *   The sequence of keys to check.
   *
   * @return bool|mixed
   *   Return false else if not found.
   */
  public function checkHierarchy($tree, array &$hierarchy) {
    // If is a leave and the hierarchy has remaining levels then is not a match.
    if (!is_array($tree) && count($hierarchy) > 0) {
      return FALSE;
    }
    // If the hierarchy is empty then it is a match.
    if (count($hierarchy) == 0) {
      return $tree;
    }
    // Gets the next level to check.
    $key = array_shift($hierarchy);
    $key = is_numeric($key) ? (int) $key : $key;
    // Check if the key exists in the hierarchy.
    $next = $tree[$key] ?? FALSE;

    // If the key matches, continue with the next level.
    if ($next) {
      return $this->checkHierarchy($next, $hierarchy);
    }
    return FALSE;
  }

  /**
   * Replace fields based on replacements definitions.
   *
   * @param mixed $tree
   *   Json decoded string into array.
   * @param string $field_name
   *   Name of the field to be replaced by the hierarchy.
   * @param array $hierarchy
   *   The sequence of keys to check.
   */
  private function expandTreeValue($tree, string $field_name, array $hierarchy) {
    if (!is_array($tree)) {
      return $tree;
    }
    // Check if array is associative.
    if (array_keys($tree) !== range(0, count($tree) - 1)) {
      $res = [];
      foreach ($tree as $key => $value) {
        // If the node is the specified field then process.
        if ($key === $field_name) {
          // Extract the value from the hierarchy if it matches.
          $matched_value = $this->checkHierarchy($tree, $hierarchy);
          if ($matched_value) {
            $final_val = $this->expandTreeValue($matched_value, $field_name, $hierarchy);
            $res = array_merge($res, $final_val);
          }
          else {
            $res[$key] = $this->expandTreeValue($value, $field_name, $hierarchy);
          }
        }
        else {
          $res[$key] = $value;
        }
      }
      return $res;
    }
    else {
      $res = [];
      foreach ($tree as $value) {
        $res[] = $this->expandTreeValue($value, $field_name, $hierarchy);
      }
      return $res;
    }

  }

}
