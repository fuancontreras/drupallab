<?php

namespace Drupal\cnbc_rest_api\Plugin\Processor;

use Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorBase;
use Drupal\Core\Cache\CacheableJsonResponse;

/**
 * Provides a CnbcApiProcessor processor.
 *
 * @CnbcApiProcessor(
 *   id = "extract",
 *   label = @Translation("Replaces the field output with the resolved
 *   hierarchy."))
 * )
 */
class CnbcFieldExtract extends CnbcApiProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function getExample() {
    return "<p># @output(<b>extract</b>, fieldWrittenBy, 0, entity, entityId)</p>";
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '<p>Input: <pre>    "fieldWrittenBy": [
      {
        "ordinal": 2,
        "targetId": 2,
        "entity": {
          "entityId": "2"
        }
      }
    ]
    <pre>Output: <pre>       "fieldWrittenBy": "2",
    }</pre></p>';
  }

  /**
   * {@inheritdoc}
   */
  public function processOutput($operations, CacheableJsonResponse $json_result, array $parameters = []) {
    if (count($parameters) > 1) {
      $key = reset($parameters);

      // Extract data.
      $data = json_decode($json_result->getContent(), TRUE);
      $data = $this->extractTreeValue($data, $key, $parameters);
      $json_result->setContent(json_encode($data));
    }
  }

  /**
   * Finds a value in a multidimensional array given a hierarchy of keys.
   *
   * @param mixed $tree
   *   Multidimensional array or value.
   * @param array $hierarchy
   *   The sequence of keys to check.
   *
   * @return bool|mixed
   *   Return false else if not found.
   */
  private function checkTreeHierarchy($tree, array &$hierarchy) {
    if (!is_array($tree) && count($hierarchy) > 0) {
      return FALSE;
    }
    if (is_array($tree) && count($hierarchy) == 0) {
      // The result must be an associative array.
      if (array_keys($tree) !== range(0, count($tree) - 1)) {
        return $tree;
      }
      else {
        return FALSE;
      }
    }
    $key = array_shift($hierarchy);
    $key = is_numeric($key) ? (int) $key : $key;
    $next = $tree[$key] ?? FALSE;
    if ($next) {
      return $this->checkTreeHierarchy($next, $hierarchy);
    }
    return FALSE;
  }

  /**
   * Replace fields based on replacements definitions.
   *
   * @param mixed $tree
   *   Json decoded string into array.
   * @param string $field_name
   *   Name of the field to be extracted.
   * @param array $hierarchy
   *   The sequence of keys to check.
   */
  private function extractTreeValue($tree, string $field_name, array $hierarchy) {
    if (!is_array($tree)) {
      return $tree;
    }
    // Check if array is associative.
    if (array_keys($tree) !== range(0, count($tree) - 1)) {
      $res = [];
      foreach ($tree as $key => $value) {
        // If the node matches the specified field name.
        if ($key == $field_name) {
          $matched_value = $this->checkTreeHierarchy($tree, $hierarchy);
          if ($matched_value) {
            $res[$key] = $this->extractTreeValue($matched_value, $field_name, $hierarchy);
          }
          else {
            $res[$key] = $this->extractTreeValue($value, $field_name, $hierarchy);
          }
        }
        else {
          $res[$key] = $value;
        }
      }
      return $res;
    }
    else {
      $res = [];
      foreach ($tree as $value) {
        $res[] = $this->extractTreeValue($value, $field_name, $hierarchy);
      }
      return $res;
    }

  }

}
