<?php

namespace Drupal\cnbc_rest_api\Plugin\Processor;

use Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorBase;
use Drupal\Core\Cache\CacheableJsonResponse;

/**
 * Provides a CnbcApiProcessor processor.
 *
 * @CnbcApiProcessor(
 *   id = "alias",
 *   label = @Translation("Replaces old aliases with new aliases.")
 * )
 */
class CnbcFieldsAlias extends CnbcApiProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '<p>Replaces field old alias with new alias for the provided fields.</p>
    <p>Assigning unique GraphQL aliases allows processors scoping. </p>
    ';
  }

  /**
   * {@inheritdoc}
   */
  public function getExample() {
    return "
        <p># @output(<b>alias</b>, oldAlias, newAlias)</p>
        <p># @output(<b>alias</b>, oldAlias1, newAlias1, 
        oldAlias2, newAlias2, ...)</p>";
  }

  /**
   * {@inheritdoc}
   *
   * Replaces the boolean fields with "Yes" "No" strings.
   */
  public function processOutput($operations, CacheableJsonResponse $json_result, array $parameters = []) {
    if (!empty($parameters)) {
      for ($i = 0; $i < count($parameters) - 1; $i += 2) {
        $old_alias = $parameters[$i];
        $new_alias = $parameters[$i + 1];
        if ($old_alias ?? ($new_alias ?? FALSE)) {
          $replacements[$old_alias] = $new_alias;
        }
      }

      // Extract data.
      if (!empty($replacements)) {
        $data = json_decode($json_result->getContent(), TRUE);
        $data = $this->replaceMatchField($data, $replacements);
        $json_result->setContent(json_encode($data));
      }
    }
  }

  /**
   * Replace fields based on replacements definitions.
   *
   * @param mixed $tree
   *   Json decoded string into array.
   * @param mixed $replacements
   *   Mixed array of definitions.
   */
  private function replaceMatchField($tree, $replacements) {
    // If value is not an array then the recursion stops.
    if (!is_array($tree)) {
      return $tree;
    }

    // Check if array is associative.
    if (array_keys($tree) !== range(0, count($tree) - 1)) {
      $res = [];
      // Only associative arrays contains aliases.
      foreach ($tree as $key => $value) {
        // If old alias exists in the replacements collection then replace key.
        if (array_key_exists($key, $replacements)) {
          $new_key = $replacements[$key];
          $res[$new_key] = $this->replaceMatchField($value, $replacements);;
        }
        else {
          $res[$key] = $this->replaceMatchField($value, $replacements);
        }
      }
      return $res;
    }
    // Indexed arrays represent values without field names.
    else {
      $res = [];
      foreach ($tree as $value) {
        $res[] = $this->replaceMatchField($value, $replacements);
      }
      return $res;
    }

  }

}
