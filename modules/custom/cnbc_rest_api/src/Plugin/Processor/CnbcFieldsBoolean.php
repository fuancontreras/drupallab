<?php

namespace Drupal\cnbc_rest_api\Plugin\Processor;

use Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorBase;
use Drupal\Core\Cache\CacheableJsonResponse;

/**
 * Provides a CnbcApiProcessor processor.
 *
 * @CnbcApiProcessor(
 *   id = "boolean",
 *   label = @Translation("Boolean fields rewrite.")
 * )
 */
class CnbcFieldsBoolean extends CnbcApiProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '<p>Replaces true/false with "Yes"/"No" for the specified fields.</p>
        <p>If no fields provided then all boolean values will be replaced.</p>    
    ';
  }

  /**
   * {@inheritdoc}
   */
  public function getExample() {
    return "
         <p># @output(<b>boolean</b>)</p>
        <p># @output(<b>boolean</b>, field)</p>
        <p># @output(<b>boolean</b>, field1, field2, field3, ...)</p>";
  }

  /**
   * {@inheritdoc}
   *
   * Replaces the boolean fields with "Yes" "No" strings.
   */
  public function processOutput($operations, CacheableJsonResponse $json_result, array $parameters = []) {
    $str_data = $json_result->getContent();
    if (count($parameters) == 0) {
      $patterns = [
        '/:(?<true>true)/m',
        '/:(?<true>false)/m',
      ];
      $replacements = [
        ':"Yes"',
        ':"No"',
      ];

      $str_data = preg_replace($patterns, $replacements, $str_data);
    }
    else {
      foreach ($parameters as $field) {
        $search[] = "\"$field\":true";
        $search[] = "\"$field\":false";
        $replacements[] = "\"$field\":\"Yes\"";
        $replacements[] = "\"$field\":\"No\"";
      }
      $str_data = str_replace($search, $replacements, $str_data);
    }
    $json_result->setContent($str_data);

  }

}
