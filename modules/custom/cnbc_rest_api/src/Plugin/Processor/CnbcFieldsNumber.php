<?php

namespace Drupal\cnbc_rest_api\Plugin\Processor;

use Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorBase;
use Drupal\Core\Cache\CacheableJsonResponse;

/**
 * Provides a CnbcApiProcessor processor.
 *
 * @CnbcApiProcessor(
 *   id = "number",
 *   label = @Translation("Number fields rewrite.")
 * )
 */
class CnbcFieldsNumber extends CnbcApiProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return 'Replaces a string numeric value like <pre>field:"123"</pre>  for the JSON numeric value format
    <pre>field:123</pre> If the value is not numeric then is not modified.
    <p>If no fields provided then all numeric string values will be replaced.</p>';
  }

  /**
   * {@inheritdoc}
   */
  public function getExample() {
    return "
        <p># @output(<b>number</b>)</p>
        <p># @output(<b>number</b>, field)</p>
        <p># @output(<b>number</b>, field1, field2, field3, ...)</p>";
  }

  /**
   * {@inheritdoc}
   *
   * Replaces the string numeric fields with JSON numeric values.
   */
  public function processOutput($operations, CacheableJsonResponse $json_result, array $parameters = []) {
    /* If field names are provided, then a collection of value checks is created
     * using field names as keys. These functions are invoked by the recursive
     * pre-order algorithm whenever the node key matches the field name. */
    if (empty($parameters)) {
      $parameters = ['*'];
    }
    // Extract data.
    $data = json_decode($json_result->getContent(), TRUE);
    $data = $this->replaceNumericField($data, $parameters);
    $json_result->setContent(json_encode($data));

  }

  /**
   * Replace fields based on replacements definitions.
   *
   * If replacements includes * key then the validation is executed on every
   * tree node.
   *
   * @param mixed $tree
   *   Json decoded string into array.
   * @param array $replacements
   *   Array of names.
   */
  private function replaceNumericField($tree, array $replacements) {
    // If the tree value is not an array it means that is a leave value.
    if (!is_array($tree)) {
      return $tree;
    }

    // Check if array is associative.
    if (array_keys($tree) !== range(0, count($tree) - 1)) {
      $res = [];
      // Check if * wildcard key is being used (String numeric fields).
      $apply_all = in_array('*', $replacements);
      foreach ($tree as $key => $value) {
        // If a validation needs to be executed via wildcard or field name.
        if ($apply_all || in_array($key, $replacements)) {
          $value = is_string($value) && is_numeric($value) ? (int) $value : $value;
          $res[$key] = $this->replaceNumericField($value, $replacements);
        }
        else {
          $res[$key] = $value;
        }
      }
      return $res;
    }
    else {
      $res = [];
      foreach ($tree as $value) {
        $res[] = $this->replaceNumericField($value, $replacements);
      }
      return $res;
    }

  }

}
