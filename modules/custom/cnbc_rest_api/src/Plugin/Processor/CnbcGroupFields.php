<?php

namespace Drupal\cnbc_rest_api\Plugin\Processor;

use Drupal\cnbc_rest_api\Plugin\CnbcApiProcessorBase;
use Drupal\Core\Cache\CacheableJsonResponse;

/**
 * Provides a CnbcApiProcessor processor.
 *
 * @CnbcApiProcessor(
 *   id = "group",
 *   label = @Translation("Group fields that share prefixes."),
 *   example = "# output(<b>group</b>)"
 * )
 */
class CnbcGroupFields extends CnbcApiProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return 'Input: <pre>    [prexix]__[fieldname]:[value]</pre>  Output:
    <pre>    [prefix] {
     [fieldname]: [value]
    }</pre>';
  }

  /**
   * {@inheritdoc}
   */
  public function processOutput($operations, CacheableJsonResponse $json_result, array $parameters = []) {
    $data = json_decode($json_result->getContent(), TRUE);
    $data = $this->groupByPrefixPreorder($data);
    $json_result->setContent(json_encode($data));
  }

  /**
   * Groups array keys into an additional array level.
   *
   * @param mixed $tree
   *   Recursive value of an array item.
   */
  private function groupByPrefixPreorder($tree) {
    // If the tree is not an array then is successful end of recursion.
    if (!is_array($tree)) {
      return $tree;
    }

    // Check if array is associative. Otherwise, recursion continues.
    if (array_keys($tree) !== range(0, count($tree) - 1)) {
      $res = [];
      foreach ($tree as $key => $value) {
        // If __ is found an extraction needs to be executed.
        if (strpos($key, '__')) {
          $split = explode('__', $key);
          $res[$split[0]][$split[1]] = $this->groupByPrefixPreorder($value);
        }
        else {
          $res[$key] = $this->groupByPrefixPreorder($value);
        }
      }
      return $res;
    }
    else {
      $res = [];
      foreach ($tree as $value) {
        $res[] = $this->groupByPrefixPreorder($value);
      }
      return $res;
    }

  }

}
