<?php

namespace Drupal\cnbc_rest_api\Plugin\rest\resource;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "graphql_custom_endpoint",
 *   label = @Translation("GraphQL custom endpoint"),
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/custom/post",
 *     "canonical" = "/custom/get/{param}",
 *   },
 *   deriver = "\Drupal\cnbc_rest_api\Plugin\Derivative\GraphQLEndpointDerivative"
 * )
 */
class GraphQLCustomEndpoint extends GraphqlEndpointBase {

  /**
   * {@inheritdoc}
   */
  public function getQuery() {
    $query = <<<GQL
    {
        nodeById(id:"@param0") {
         title
         entityOwner {
           uid
           uuid
           name
         }
        }
      }
GQL;
    return $query;
  }

}
