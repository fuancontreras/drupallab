<?php

namespace Drupal\cnbc_rest_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\cnbc_rest_api\CnbcRestApiService;

/**
 * Class GraphqlEndpointBase.
 */
abstract class GraphqlEndpointBase extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * A graphql query processor.
   *
   * @var \Drupal\cnbc_rest_api\CnbcRestApiService
   */
  protected $connect;

  /**
   * Drupal\Core\Entity\EntityStorageInterface manager.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $endpointsConfigStorage;

  /**
   * Constructs a new GraphQLCustomEndpoint object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\cnbc_rest_api\CnbcRestApiService $connect
   *   A graphql query processor.
   * @param \Drupal\Core\Entity\EntityStorageInterface $cnbc_config_endpoint_storage
   *   The GraphQL endpoint resource config storage.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    CnbcRestApiService $connect,
    EntityStorageInterface $cnbc_config_endpoint_storage
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->connect = $connect;
    $this->endpointsConfigStorage = $cnbc_config_endpoint_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('cnbc_rest_api'),
      $container->get('current_user'),
      $container->get('cnbc_rest_api.connect'),
      $container->get('entity_type.manager')
        ->getStorage('graphql_endpoint_config')
    );
  }

  /**
   * Process request via GraphQL.
   *
   * @param mixed $args
   *   Function paramaters.
   *
   * @return \Drupal\rest\ResourceResponse
   *   Service response object
   */
  public function processGraphQlQuery(...$args) {
    $request = array_pop($args);
    $index = 1;
    $url_params = [];
    $get_params = [];

    // Get URL parameters.
    foreach ($args as $param) {
      if ($param) {
        $url_params["param$index"] = $param;
        $index++;
      }
    }

    // Get Query parameters.
    foreach ($request->query as $key => $value) {
      $get_params["$key"] = $value;
    }

    $entity = $this->endpointsConfigStorage->load($this->pluginDefinition['id']);

    // Get query.
    $query = ($entity) ? $entity->query : $this->getQuery();

    $query_result = $this->connect->executeGraphQlQuery($query, $url_params + $get_params);
    return $query_result;
  }

  /**
   * Responds to entity GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   Service response object
   */
  public function get(...$args) {
    return $this->processGraphQlQuery(...$args);
  }

  /**
   * Returns the gcl query string.
   *
   * @return string
   *   The gql string to be used by default.
   */
  abstract public function getQuery();

}
