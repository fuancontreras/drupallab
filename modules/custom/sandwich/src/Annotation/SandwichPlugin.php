<?php

namespace Drupal\sandwich\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Sandich plugin item annotation object.
 *
 * @see \Drupal\sandwich\Plugin\SandichPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class SandwichPlugin extends Plugin {


  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A himan readable description of the sandwich.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $decription;

  /**
   * The number of calories per serving of this sandich type.
   *
   * @var float
   */
  public $calories;

}
