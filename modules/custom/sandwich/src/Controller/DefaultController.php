<?php

namespace Drupal\sandwich\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\graphql\GraphQL\Execution\QueryProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\sandwich\Plugin\SandwichPluginManager;
use Drupal\graphql\Controller\RequestController;
use GraphQL\Server\OperationParams;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   * Drupal\sandwich\Plugin\SandwichPluginManager definition.
   *
   * @var \Drupal\sandwich\Plugin\SandwichPluginManager
   */
  protected $pluginManagerSandwichPlugin;

  /**
   * Constructs a new DefaultController object.
   */
  public function __construct(SandwichPluginManager $plugin_manager_sandwich_plugin, QueryProcessor $processor, array $parameters) {
    $this->pluginManagerSandwichPlugin = $plugin_manager_sandwich_plugin;
    $this->processor = $processor;
    $this->parameters = $parameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.sandwich_plugin'),
      $container->get('graphql.query_processor'),
      $container->getParameter('graphql.config')
    );
  }

  /**
   * Getdefinitions.
   *
   * @return string
   *   Return Hello string.
   */
  public function getDefinitions() {
    // Get the list of all the sandwich plugins defined on the system from the
    // plugin manager. Note that at this point, what we have is *definitions* of
    // plugins, not the plugins themselves.
    $sandwich_plugin_definitions = $this->pluginManagerSandwichPlugin->getDefinitions();

    // Let's output a list of the plugin definitions we now have.
    $items = [];
    foreach ($sandwich_plugin_definitions as $sandwich_plugin_definition) {
      // Here we use various properties from the plugin definition. These values
      // are defined in the annotation at the top of the plugin class: see
      // \Drupal\plugin_type_example\Plugin\Sandwich\ExampleHamSandwich.
      $items[] = $this->t("@id (calories: @calories, description: @description )", [
        '@id' => $sandwich_plugin_definition['id'],
        '@calories' => $sandwich_plugin_definition['calories'],
        '@description' => $sandwich_plugin_definition['description'],
      ]);
    }

    // Add our list to the render array.
    $build['plugin_definitions'] = [
      '#theme' => 'item_list',
      '#title' => 'Sandwich plugin definitions',
      '#items' => $items,
    ];
    return $build;
  }

  /**
   * Createhamsandwichinstance.
   *
   * @return string
   *   Return Hello string.
   */
  public function createInstances() {
    // To get an instance of a plugin, we call createInstance() on the plugin
    // manager, passing the ID of the plugin we want to load. Let's output a
    // list of the plugins by loading an instance of each plugin definition and
    // collecting the description from each.
    $items = [];
    // The array of plugin definitions is keyed by plugin id, so we can just use
    // that to load our plugin instances.
    $sandwich_plugin_definitions = $this->pluginManagerSandwichPlugin->getDefinitions();
    foreach ($sandwich_plugin_definitions as $plugin_id => $sandwich_plugin_definition) {
      // We now have a plugin instance. From here on it can be treated just as
      // any other object; have its properties examined, methods called, etc.
      $plugin = $this->pluginManagerSandwichPlugin->createInstance($plugin_id);
      $definition = $plugin->getPluginDefinition();
      $items[] = $definition['description'] . " (" . $definition['calories'] . ") calories";
    }

    // Add our list to the render array.
    $build['plugin_definitions'] = [
      '#theme' => 'item_list',
      '#title' => 'Sandwich plugin instances',
      '#items' => $items,
    ];
    return $build;
  }

  /**
   * Graphqlrequest.
   *
   * @return string
   *   Return Hello string.
   */
  public function graphqlRequest() {
    $controller = new RequestController($this->processor, $this->parameters);
    $query = new OperationParams([]);
    $query->query = <<<GQL
     {
        nodeById(id:"11") {
         title
         entityOwner {
           uid
           uuid
           name
         }
          ... on NodeArticle {
            fieldImage {
              url
            }
          }
        }
      }
GQL;
    $json = $controller->handleRequest('default:default', $query);
    $data = $json->getContent();
    $response = json_encode($data, JSON_PRETTY_PRINT);
    return [
      '#type' => 'markup',
      '#markup' => "<pre>$response</pre>",
    ];
  }

}
