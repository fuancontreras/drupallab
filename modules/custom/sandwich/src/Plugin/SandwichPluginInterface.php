<?php

namespace Drupal\sandwich\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Sandich plugin plugins.
 */
interface SandwichPluginInterface extends PluginInspectionInterface {

  /**
   * Provide a description of the sandwich.
   *
   * @return string
   *   A string description of the sandwich.
   */
  public function getDescription();

  /**
   * Provide the number of calories per serving for the sandwich.
   *
   * @return float
   *   The number of calories per serving.
   */
  public function getCalories();

  /**
   * Order a sandwich.
   *
   * @param array $extras
   *   An array of extra ingredients to include with this sandwich.
   *
   * @return mixed
   *   The sandwhich order element.
   */
  public function order(array $extras);

}
